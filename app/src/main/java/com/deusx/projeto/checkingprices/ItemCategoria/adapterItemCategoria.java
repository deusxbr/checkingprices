package com.deusx.projeto.checkingprices.ItemCategoria;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.deusx.projeto.checkingprices.Helper.ItemCategoriaComparator;
import com.deusx.projeto.checkingprices.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by MacPro-DeusX on 15/04/15.
 */
public class adapterItemCategoria extends ArrayAdapter<ItemCategoriaModel> {

    int resource;
    List<ItemCategoriaModel> data;

    public adapterItemCategoria(Context context, int resource, List<ItemCategoriaModel> items) {
        super(context, resource);
        this.resource = resource;
        sortData(items);
        this.data = items;
    }

    public void updateDataList(List<ItemCategoriaModel> newData){
        data.clear();
        sortData(newData);
        data.addAll(newData);
        this.notifyDataSetChanged();
    }

    public void sortData(List<ItemCategoriaModel> itens){
        Collections.sort(itens, new ItemCategoriaComparator()); //ordena os dados
    }

    @Override
    public ItemCategoriaModel getItem(int position){
        return data.get(position);
    }

    @Override
    public int getCount(){
        return data.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LinearLayout categoriaView;

        if(convertView == null) {
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater vi;
            vi = (LayoutInflater) getContext().getSystemService(inflater);
            convertView = vi.inflate(resource, parent, false);
        }

        ItemCategoriaModel itemCategoriaModel = (ItemCategoriaModel) getItem(position);

        TextView tvloja = (TextView) convertView.findViewById(R.id.tvloja);
        tvloja.setText(itemCategoriaModel.getLoja());

        TextView tvvalor = (TextView) convertView.findViewById(R.id.tvvalor);
        tvvalor.setText(itemCategoriaModel.getValor());

        return convertView;
    }
}
