package com.deusx.projeto.checkingprices.Categoria;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.deusx.projeto.checkingprices.BDHelper;
import com.deusx.projeto.checkingprices.R;

import java.util.ArrayList;
import java.util.List;


public class addCategoriaActivity extends ActionBarActivity implements View.OnClickListener {

    EditText name, id;
    Button addButton, deleteButton;
    TextView tv;
    List<CategoriaModel> list = new ArrayList<CategoriaModel>();
    BDHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_categoria);

        db = new BDHelper(getApplicationContext());

        name = (EditText) findViewById(R.id.editText1);
        addButton = (Button) findViewById(R.id.add);
        tv = (TextView) findViewById(R.id.tv);

        addButton.setOnClickListener(this);
    }


    private void print(List<CategoriaModel> list) {
        // TODO Auto-generated method stub
        String value = "";
        for(CategoriaModel sm : list){
            value = value + "id: " + sm.getId() + ", name: "+sm.getName()+"\n";
        }
        tv.setText(value);
    }

    @Override
    public void onClick(View v) {
        if(v == findViewById(R.id.add)){
            boolean error = false;

            final String loja = name.getText().toString();
            if( !isValidName(loja) ){
                name.setError("Campo nome é obrigatório");
                error = true;
                name.requestFocus();
            }else{
                name.setError(null);
            }

            Log.d("tag", "Erro: " + error);

            if(!error){
                tv.setText("");

                CategoriaModel categoria = new CategoriaModel();
                categoria.setName(name.getText().toString());

                db.addCategoriaDetail(categoria);

                this.finish();
            }


        }
    }


    private boolean isValidName(String name) {
        Log.d("tag", "" + name.trim().length() );
        if(name.trim().length() != 0){
            return true;
        }
        return false;
    }
}
