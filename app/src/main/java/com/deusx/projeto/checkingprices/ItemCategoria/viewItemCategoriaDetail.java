package com.deusx.projeto.checkingprices.ItemCategoria;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.deusx.projeto.checkingprices.BDHelper;
import com.deusx.projeto.checkingprices.R;

public class viewItemCategoriaDetail extends ActionBarActivity {

    BDHelper db;
    ItemCategoriaModel itemCategoriaModel;
    int itemCategoriaId;

    TextView tvloja;
    TextView tvvalor;
    TextView tvpagamento;
    TextView tvnota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item_categoria_detail);

        db = new BDHelper(getApplicationContext());

        itemCategoriaId = Integer.valueOf(getIntent().getStringExtra("itemCategoriaId"));

        itemCategoriaModel = db.getItemCategoria(itemCategoriaId);

        tvloja = (TextView) findViewById(R.id.tvloja);
        tvvalor = (TextView) findViewById(R.id.tvvalor);
        tvpagamento = (TextView) findViewById(R.id.tvpagamento);
        tvnota = (TextView) findViewById(R.id.tvnota);

        tvloja.setText(itemCategoriaModel.getLoja());
        tvvalor.setText(itemCategoriaModel.getValor());
        tvpagamento.setText(itemCategoriaModel.getPagamento());
        tvnota.setText(itemCategoriaModel.getNotas());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_item_categoria_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deletarItemCategoria:

                new AlertDialog.Builder(this)
                        .setTitle("Deletar Item")
                        .setMessage("Você tem certeza que gostaria de deletar este item de categoria?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        db = new BDHelper(getApplicationContext());
                                        db.deleteItemCategoria(itemCategoriaId);
                                        finish();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                return true;
        }
        return false;
    }
}
