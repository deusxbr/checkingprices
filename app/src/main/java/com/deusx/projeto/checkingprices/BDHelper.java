package com.deusx.projeto.checkingprices;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.deusx.projeto.checkingprices.Categoria.CategoriaModel;
import com.deusx.projeto.checkingprices.ItemCategoria.ItemCategoriaModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MacPro-DeusX on 05/04/15.
 */
public class BDHelper extends SQLiteOpenHelper {

    // Database Name
    public static String DATABASE_NAME = "orcamentoBD";
    private static final int DATABASE_VERSION = 1;

    // Dados tabela categoria
    private static final String TABLE_CATEGORIA = "categoria";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";

    // Dados tabela itens categoria
    private static final String TABLE_ITENS_CATEGORIA = "itens_categoria";
    private static final String KEY_ITENS_CATEGORIA_ID = "id";
    private static final String KEY_ITENS_CATEGORIA_LOJA = "loja";
    private static final String KEY_ITENS_CATEGORIA_VALOR = "valor";
    private static final String KEY_ITENS_CATEGORIA_PAGAMENTO = "pagamento";
    private static final String KEY_ITENS_CATEGORIA_NOTAS = "notas";
    private static final String KEY_ITENS_CATEGORIA_CATEGORIA_ID = "categoria_id";

    public static String TAG = "tag";

    SQLiteDatabase db;


    // Criacao das tabelas
    private static final String CREATE_TABLE_CATEGORIAS = "CREATE TABLE "
                                                          + TABLE_CATEGORIA + "("
                                                          + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                                                          + KEY_NAME + " TEXT"
                                                          + ");";

    private static final String CREATE_TABLE_ITENS_CATEGORIAS = "CREATE TABLE "
                                                          + TABLE_ITENS_CATEGORIA + "("
                                                          + KEY_ITENS_CATEGORIA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                                                          + KEY_ITENS_CATEGORIA_LOJA + " TEXT,"
                                                          + KEY_ITENS_CATEGORIA_VALOR + " TEXT,"
                                                          + KEY_ITENS_CATEGORIA_PAGAMENTO + " TEXT,"
                                                          + KEY_ITENS_CATEGORIA_NOTAS + " TEXT,"
                                                          + KEY_ITENS_CATEGORIA_CATEGORIA_ID + " INTEGER"
                                                          + ");";


    /**
     * Construtor da classe
     * @param context
     */
    public BDHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Cria-se as tabelas
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CATEGORIAS);
        db.execSQL(CREATE_TABLE_ITENS_CATEGORIAS);
    }

    /**
     * Faz o upgrade da versao do banco de dados, caso ha modificacoes na estrutura
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_CATEGORIAS);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_ITENS_CATEGORIAS);

        onCreate(db);
    }


    /**
     * Abre conexao com o banco
     * Modo de escrita
     * @return
     */
    public BDHelper openWrite(){
        db = this.getWritableDatabase();
        return this;
    }

    /**
     * Abre conexao com o banco
     * Modo de leitura
     * @return
     */
    public BDHelper openRead(){
        db = this.getReadableDatabase();
        return this;
    }

    /**
     *  Metodo com problema
     */
    @Deprecated
    public void close(){
        //this.close();
    }



    //*******************   Metodos para tabela Categoria   *******************

    /**
     *
     * This method is used to add students detail in students Table
     *
     * @param categoria
     * @return
     */
    public long addCategoriaDetail(CategoriaModel categoria) {
        openWrite();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, categoria.getName());

        long insert = db.insert(TABLE_CATEGORIA, null, values);

        close();

        return insert;
    }

    /**
     * This method is used to update particular student entry
     *
     * @param categoria
     * @return
     */
    public int updateEntry(CategoriaModel categoria) {
        openWrite();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, categoria.getName());

        close();

        // update row in students table base on students.is value
        return db.update(TABLE_CATEGORIA, values, KEY_ID + " = ?",
                new String[] { String.valueOf(categoria.getId()) });
    }

    /**
     * Metodo deleta uma categoria
     *
     * @param id
     */
    public void deleteEntry(long id) {
        openWrite();

        db.delete(TABLE_CATEGORIA, KEY_ID + " = ?",
                new String[] { String.valueOf(id) });

        close();
    }


    /**
     * Deleta tabela Categoria
     * Deleta em cascade a tabela itens categoria
     *
     */
    public void clearTable(){
        openWrite();
        db.delete(TABLE_CATEGORIA, null, null);
        db.delete(TABLE_ITENS_CATEGORIA, null, null);
        close();
    }

    /**
     * Metodo para excluir todas as categorias marcadas no checkbox
     *
     * @param itensId
     */
    public void deleteAllCategoriaChecked(List<Integer> itensId ) {
        openWrite();

        int count = itensId.size();

        for (int i = 0; i < count; i++) {
            db.delete(TABLE_CATEGORIA, KEY_ID + " = ?",
                    new String[] { String.valueOf(itensId.get(i)) });

            deleteItemCategoriaItemCategoriaId(itensId.get(i));
        }

        close();
    }

    /**
     * Retorna uma categoria especifica
     *
     * @param id
     * @return
     */
    public CategoriaModel getCategoria(long id) {
        openRead();

        String selectQuery = "SELECT * FROM " + TABLE_CATEGORIA + " WHERE "
                + KEY_ID + " = " + id;

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        CategoriaModel categorias = new CategoriaModel();
        categorias.setId(c.getInt(c.getColumnIndex(KEY_ID)));
        categorias.setName(c.getString(c.getColumnIndex(KEY_NAME)));

        close();

        return categorias;
    }

    /**
     * Retorna todas as categorias
     *
     * @return
     */
    public List<CategoriaModel> getAllCategoriasList() {

        List<CategoriaModel> categoriaArrayList = new ArrayList<CategoriaModel>();

        String selectQuery = "SELECT * FROM " + TABLE_CATEGORIA;

        openRead();

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                CategoriaModel categoria = new CategoriaModel();
                categoria.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                categoria.setName(c.getString(c.getColumnIndex(KEY_NAME)));

                categoriaArrayList.add(categoria);
            } while (c.moveToNext());
        }

        close();

        return categoriaArrayList;
    }




    //*******************   Metodos para tabela itens_categoria   *******************


    /**
     *
     * Metodo para adicionar um item na tabela itens de categorias
     *
     * @param itemCategoria
     * @return
     */
    public long addItemCategoria(ItemCategoriaModel itemCategoria) {
        openWrite();

        ContentValues values = new ContentValues();
        values.put(KEY_ITENS_CATEGORIA_LOJA, itemCategoria.getLoja());
        values.put(KEY_ITENS_CATEGORIA_VALOR, itemCategoria.getValor());
        values.put(KEY_ITENS_CATEGORIA_PAGAMENTO, itemCategoria.getPagamento());
        values.put(KEY_ITENS_CATEGORIA_NOTAS, itemCategoria.getNotas());
        values.put(KEY_ITENS_CATEGORIA_CATEGORIA_ID, itemCategoria.getCategoria_id());

        long insert = db.insert(TABLE_ITENS_CATEGORIA, null, values);

        close();

        return insert;
    }

    /**
     * Retorna todos os itens de categoria
     *
     * @param categoria_id
     * @return
     */
    public List<ItemCategoriaModel> getAllItemCategoriasList(int categoria_id) {

        List<ItemCategoriaModel> itemCategoriaArrayList = new ArrayList<ItemCategoriaModel>();

        String selectQuery = "SELECT * FROM " + TABLE_ITENS_CATEGORIA
                            + " WHERE " + KEY_ITENS_CATEGORIA_CATEGORIA_ID + " = " + categoria_id;

        openRead();

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                ItemCategoriaModel itemCategoria = new ItemCategoriaModel();

                itemCategoria.setId( c.getInt(c.getColumnIndex(KEY_ITENS_CATEGORIA_ID)) );
                itemCategoria.setLoja( c.getString(c.getColumnIndex(KEY_ITENS_CATEGORIA_LOJA)) );
                itemCategoria.setValor( c.getString(c.getColumnIndex(KEY_ITENS_CATEGORIA_VALOR)) );
                itemCategoria.setPagamento( c.getString(c.getColumnIndex(KEY_ITENS_CATEGORIA_PAGAMENTO)) );
                itemCategoria.setNotas( c.getString(c.getColumnIndex(KEY_ITENS_CATEGORIA_NOTAS)) );
                itemCategoria.setCategoria_id( c.getInt(c.getColumnIndex(KEY_ITENS_CATEGORIA_CATEGORIA_ID)) );

                itemCategoriaArrayList.add(itemCategoria);
            } while (c.moveToNext());
        }

        close();

        return itemCategoriaArrayList;
    }

    /**
     * Retorna um item de categoria especifico
     *
     * @param id
     * @return
     */

    public ItemCategoriaModel getItemCategoria(long id) {
        openRead();

        String selectQuery = "SELECT * FROM " + TABLE_ITENS_CATEGORIA + " WHERE "
                + KEY_ITENS_CATEGORIA_ID + " = " + id;

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        ItemCategoriaModel itemCategorias = new ItemCategoriaModel();
        itemCategorias.setId(c.getInt(c.getColumnIndex(KEY_ID)));
        itemCategorias.setLoja(c.getString(c.getColumnIndex(KEY_ITENS_CATEGORIA_LOJA)));
        itemCategorias.setValor(c.getString(c.getColumnIndex(KEY_ITENS_CATEGORIA_VALOR)));
        itemCategorias.setPagamento(c.getString(c.getColumnIndex(KEY_ITENS_CATEGORIA_PAGAMENTO)));
        itemCategorias.setNotas(c.getString(c.getColumnIndex(KEY_ITENS_CATEGORIA_NOTAS)));

        close();

        return itemCategorias;
    }

    /**
     * Metodo para deletar um item de categoria
     *
     * @param id
     */
    public void deleteItemCategoria(long id) {
        openWrite();

        db.delete(TABLE_ITENS_CATEGORIA, KEY_ITENS_CATEGORIA_ID + " = ?",
                new String[] { String.valueOf(id) });

        close();
    }

    /**
     * Metodo para deletar um item de categoria a partir da chave estrangeira
     *
     * @param item_categoria_id
     */
    public void deleteItemCategoriaItemCategoriaId(long item_categoria_id) {
        openWrite();

        db.delete(TABLE_ITENS_CATEGORIA, KEY_ITENS_CATEGORIA_CATEGORIA_ID + " = ?",
                new String[] { String.valueOf(item_categoria_id) });

        close();
    }


    /**
     *  Metodo para excluir todos os itens de categorias marcadas no checkbox
     *
     * @param itensId
     */
    public void deleteAllItemCategoriaChecked(List<Integer> itensId ) {
        openWrite();

        int count = itensId.size();

        for (int i = 0; i < count; i++) {
            db.delete(TABLE_ITENS_CATEGORIA, KEY_ITENS_CATEGORIA_ID + " = ?",
                    new String[] { String.valueOf(itensId.get(i)) });
        }

        close();
    }

}
