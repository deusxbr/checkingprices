package com.deusx.projeto.checkingprices.ItemCategoria;

import java.io.Serializable;

/**
 * Created by MacPro-DeusX on 15/04/15.
 */
public class ItemCategoriaModel implements Serializable {

    private int id;
    private String loja;
    private String valor;
    private String pagamento;
    private String notas;
    private int categoria_id;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLoja() {
        return loja;
    }

    public void setLoja(String loja) {
        this.loja = loja;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getPagamento() {
        return pagamento;
    }

    public void setPagamento(String pagamento) {
        this.pagamento = pagamento;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public int getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(int categoria_id) {
        this.categoria_id = categoria_id;
    }

    @Override
    public String toString(){
        return this.id + "  " + this.loja + " " + this.valor + " " + this.pagamento + " " + this.notas;
    }
}
