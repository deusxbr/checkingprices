package com.deusx.projeto.checkingprices;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.deusx.projeto.checkingprices.Categoria.CategoriaModel;
import com.deusx.projeto.checkingprices.Categoria.adapterCategoria;
import com.deusx.projeto.checkingprices.Categoria.addCategoriaActivity;
import com.deusx.projeto.checkingprices.ItemCategoria.ItemCategoriaMain;
import com.deusx.projeto.checkingprices.ItemCategoria.addItemCategoriaActivity;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    ListView lista;

    List<Integer> listCategoriaChecked;

    List<CategoriaModel> listCategoria = new ArrayList<CategoriaModel>();
    com.deusx.projeto.checkingprices.Categoria.adapterCategoria adapterCategoria;

    BDHelper db;

    public final static String SERIALIZABLE_KEY = "serializable.1212";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.title_categorias);
        setContentView(R.layout.activity_main);

    }

    @Override
    public void onResume(){
        super.onResume();

        // pegando os dados do banco e setando no categoria adpter
        db = new BDHelper(getApplicationContext());
        listCategoria = db.getAllCategoriasList();

        adapterCategoria = new adapterCategoria(this, R.layout.listitems_categoria, listCategoria);

        // atributos do ListView
        lista = (ListView) findViewById(R.id.listView1);

        // se o adapter estiver vazio mostrar textView empty
        TextView tvEmpty = (TextView) findViewById(android.R.id.empty);
        if(adapterCategoria.getCount() == 0){
            tvEmpty.setVisibility(TextView.VISIBLE);
            lista.setEmptyView(tvEmpty);
        }else{
            tvEmpty.setVisibility(TextView.GONE);
            lista.setEmptyView(tvEmpty);
        }
        //lista.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE); //usuário pode selecionar vários itens da lista
        lista.setChoiceMode(ListView.CHOICE_MODE_SINGLE);//usuário pode selecionar um item da lista
        lista.setTextFilterEnabled(true); //filtra a lista
        lista.setAdapter(adapterCategoria);

        lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                CategoriaModel categoriaModel = (CategoriaModel) (lista.getItemAtPosition(position));

                Intent myIntent = new Intent(getApplication(), addItemCategoriaActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(SERIALIZABLE_KEY, categoriaModel);
                myIntent.putExtras(bundle);
                startActivity(myIntent);

                return true;
            }
        });


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                CategoriaModel categoriaModel = (CategoriaModel) (lista.getItemAtPosition(position));

                Intent myIntent = new Intent(getApplication(), ItemCategoriaMain.class);

                Bundle bundle = new Bundle();
                bundle.putSerializable(SERIALIZABLE_KEY, categoriaModel);
                myIntent.putExtras(bundle);
                startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addCategoria:
                startActivity(new Intent(this, addCategoriaActivity.class));
                return true;

            case R.id.limparTabela:

                int count = lista.getChildCount();

                for (int i = 0; i < count; i++) {
                    View child = lista.getChildAt(i);
                    CheckBox cb = (CheckBox) child.findViewById(R.id.checkBoxDeletar);

                    if(cb.getVisibility() == CheckBox.VISIBLE){
                        db = new BDHelper(getApplicationContext());
                        if(cb.isChecked()){
                            listCategoriaChecked.add(adapterCategoria.getItem(i).getId());
                        }
                        cb.setVisibility(CheckBox.GONE);
                    }else{
                        listCategoriaChecked = new ArrayList<Integer>();
                        if(listCategoriaChecked.size() > 0){
                            listCategoriaChecked.clear();
                        }
                        cb.setChecked(false);
                        cb.setVisibility(CheckBox.VISIBLE);
                    }
                }

                if(listCategoriaChecked.size() > 0){
                    db.deleteAllCategoriaChecked(listCategoriaChecked);
                    adapterCategoria.clear();
                    adapterCategoria.updateDataList(db.getAllCategoriasList());
                }

//                new AlertDialog.Builder(this)
//                        .setTitle("Limpar Dados")
//                        .setMessage("Você tem certeza que gostaria de deletar todas as categorias?")
//                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                db = new BDHelper(getApplicationContext());
//                                db.clearTable();
//                                adapterCategoria.clear();
//                            }
//                        })
//                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                // do nothing
//                            }
//                        })
//                        .setIcon(android.R.drawable.ic_dialog_alert)
//                        .show();
//
//                return true;
        }
        return false;
    }


}
