package com.deusx.projeto.checkingprices.Categoria;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.deusx.projeto.checkingprices.R;

import java.util.List;

/**
 * Created by MacPro-DeusX on 06/04/15.
 */
public class adapterCategoria extends ArrayAdapter<CategoriaModel> {

    int resource;
//    String response;
//    Context context;
    List<CategoriaModel> data;

    public adapterCategoria(Context context, int resource, List<CategoriaModel> items) {
        super(context, resource, items);
        this.resource = resource;

        data = items;
    }

    public void updateDataList(List<CategoriaModel> newData){
        data.clear();
        data.addAll(newData);
        this.notifyDataSetChanged();
    }

    @Override
    public CategoriaModel getItem(int position){
        return data.get(position);
    }

    @Override
    public int getCount(){
        return data.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LinearLayout categoriaView;

        if(convertView == null) {
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater vi;
            vi = (LayoutInflater) getContext().getSystemService(inflater);
            convertView = vi.inflate(resource, parent, false);
        }

        CategoriaModel categoriaModel = (CategoriaModel) getItem(position);

        TextView tv = (TextView) convertView.findViewById(R.id.txtCategoriaNome);
        tv.setText(categoriaModel.getName());

        return convertView;
    }


}
