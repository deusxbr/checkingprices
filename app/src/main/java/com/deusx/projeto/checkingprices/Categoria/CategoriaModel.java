package com.deusx.projeto.checkingprices.Categoria;

import android.util.Log;

import java.io.Serializable;

/**
 * Created by MacPro-DeusX on 05/04/15.
 */
public class CategoriaModel implements Serializable{

    private int id;
    private String name;

    public CategoriaModel(int id, String name) {
        // TODO Auto-generated constructor stub
        this.id = id;
        this.name = name;
    }
    public CategoriaModel(){

    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name.substring(0,1).toUpperCase() + name.substring(1);
    }

    @Override
    public String toString() {
        return this.id + "  " + this.name;
    }

}
