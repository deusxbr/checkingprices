package com.deusx.projeto.checkingprices.ItemCategoria;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.deusx.projeto.checkingprices.BDHelper;
import com.deusx.projeto.checkingprices.Categoria.CategoriaModel;
import com.deusx.projeto.checkingprices.MainActivity;
import com.deusx.projeto.checkingprices.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class addItemCategoriaActivity extends ActionBarActivity implements View.OnClickListener {

    CategoriaModel categoriaModel;
    BDHelper db;

    TextView tvloja;
    TextView tvvalor;
    TextView tvpagamento;
    TextView tvnota;

    EditText etloja;
    EditText etvalor;
    EditText etpagamento;
    EditText etnota;

    Button buttonTirarFoto;
    Button addButton;

    ImageView imagemTirada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Categoria selecionada"); //parametro do construtor onCreate
        setContentView(R.layout.activity_add_item_categoria);

        db = new BDHelper(getApplicationContext());

        // pega o objecto quando clicado no lista de categoria
        categoriaModel = (CategoriaModel) getIntent().getSerializableExtra(MainActivity.SERIALIZABLE_KEY);
        setTitle(categoriaModel.getName());
    }

    @Override
    public void onResume(){
        super.onResume();

        tvloja = (TextView) findViewById(R.id.tvloja);
        tvvalor = (TextView) findViewById(R.id.tvvalor);
        tvpagamento = (TextView) findViewById(R.id.tvpagamento);
        tvnota = (TextView) findViewById(R.id.tvnota);

        etloja = (EditText) findViewById(R.id.etloja);
        etvalor = (EditText) findViewById(R.id.etvalor);


        //etvalor.addTextChangedListener(Mask.insert("###.###.###-##", etvalor));

        etpagamento = (EditText) findViewById(R.id.etpagamento);
        etnota = (EditText) findViewById(R.id.etnota);

        buttonTirarFoto = (Button) findViewById(R.id.buttonFoto);

//        buttonTirarFoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                intent.putExtra(MediaStore.EXTRA_OUTPUT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI.getPath());
//
//                startActivityForResult(intent, 1);
//            }
//        });

        addButton = (Button) findViewById(R.id.add);
        addButton.setOnClickListener(this);
    }

    protected void OnActivityResult(int requestCode, int resultCode, Intent data){
//        if (requestCode == 1) {
//            if(resultCode == RESULT_OK){
//                Bitmap photo = (Bitmap) data.getExtras().get("data");
//                imagemTirada = (ImageView) findViewById(R.id.imageView);
//                imagemTirada.setImageBitmap(photo);
//
//            }
//
//            if (resultCode == RESULT_CANCELED) {
//
//            }
//        }
    }

    @Override
    public void onClick(View v) {
        if(v == findViewById(R.id.add)){
            boolean errors = false;

            final String loja = etloja.getText().toString();
            if( !isValidLoja(loja) ){
                etloja.setError("Campo loja é obrigatório");
                errors = true;
                etloja.requestFocus();
            }else{
                etloja.setError(null);
            }

            final String valor = etvalor.getText().toString();
            if( !isValidValor(valor)){
                etvalor.setError("Campo valor é obrigatório \n - Exemplo: 0000,00");
                errors = true;
                etvalor.requestFocus();
            }else{
                etvalor.setError(null);
            }

            if(!errors){
                //tv.setText("");
                ItemCategoriaModel itemCategoria = new ItemCategoriaModel();
                itemCategoria.setLoja(etloja.getText().toString());
                itemCategoria.setValor(etvalor.getText().toString());
                itemCategoria.setPagamento(etpagamento.getText().toString());
                itemCategoria.setNotas(etnota.getText().toString());
                itemCategoria.setCategoria_id(categoriaModel.getId());

                db.addItemCategoria(itemCategoria);

                this.finish();
            }

        }
    }


    private boolean isValidLoja(String loja) {
        if(loja.trim().length() != 0){
            return true;
        }
        return false;
    }

    private boolean isValidValor(String valor) {
        String valor_pattern = "^(([1-9][0-9]*|00|0)\\,([0-9]{3}|[0-9]{2}))$";

        //String valor_pattern = "^(([0-9]*)\\,([0-9]{2}|[0-9]{3}))$";

        //String valor_pattern = "^[+-]?[0-9]{1,3}(?:[0-9]*(?:[.,][0-9]{2})?|(?:,[0-9]{3})*(?:\\.[0-9]{2})?|(?:\\.[0-9]{3})*(?:,[0-9]{2})?)$";

//        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
//                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(valor_pattern);
        Matcher matcher = pattern.matcher(valor);

        return matcher.matches();
    }
}
