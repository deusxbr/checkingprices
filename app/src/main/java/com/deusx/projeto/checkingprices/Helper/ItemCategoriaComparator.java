package com.deusx.projeto.checkingprices.Helper;

import com.deusx.projeto.checkingprices.ItemCategoria.ItemCategoriaModel;

import java.util.Comparator;

/**
 * Created by MacDesktop on 15/04/15.
 */
public class ItemCategoriaComparator implements Comparator<ItemCategoriaModel> {

    /**
     * Ordena os dados pelo menor valor
     * Primeiro pega a parte inteira, caso for igual testa a parte decimal
     * @param atual
     * @param proximo
     * @return
     */
    @Override
    public int compare(ItemCategoriaModel atual, ItemCategoriaModel proximo) {

        int positionAtualComa = atual.getValor().lastIndexOf(",");
        int atualInteiro = Integer.parseInt(atual.getValor().substring(0, positionAtualComa));

        int positionProximoComa = proximo.getValor().lastIndexOf(",");
        int proximoInteiro = Integer.parseInt(proximo.getValor().substring(0, positionProximoComa));

        int resultInteiro = atualInteiro - proximoInteiro;

        if(resultInteiro == 0){
            int atualDecimal = Integer.parseInt(atual.getValor().substring((positionAtualComa + 1), atual.getValor().length()));
            int proximoDecimal= Integer.parseInt(proximo.getValor().substring((positionProximoComa + 1), proximo.getValor().length()));

            return atualDecimal - proximoDecimal;
        }

        return resultInteiro;
    }
}
