package com.deusx.projeto.checkingprices.ItemCategoria;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.deusx.projeto.checkingprices.BDHelper;
import com.deusx.projeto.checkingprices.Categoria.CategoriaModel;
import com.deusx.projeto.checkingprices.MainActivity;
import com.deusx.projeto.checkingprices.R;

import java.util.ArrayList;
import java.util.List;

public class ItemCategoriaMain extends ActionBarActivity {

    ListView lista;

    List<Integer> listItemCategoriaChecked;

    List<ItemCategoriaModel> listItemCategoria;
    CategoriaModel categoriaModel;
    BDHelper db;

    com.deusx.projeto.checkingprices.ItemCategoria.adapterItemCategoria adapterItemCategoria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_item_categoria);

        db = new BDHelper(getApplicationContext());

        // pega o objecto quando clicado no lista de categoria
        categoriaModel = (CategoriaModel) getIntent().getSerializableExtra(MainActivity.SERIALIZABLE_KEY);
        setTitle("Itens da categoria " + categoriaModel.getName());
    }

    @Override
    public void onResume(){
        super.onResume();

        db = new BDHelper(getApplicationContext());
        listItemCategoria = db.getAllItemCategoriasList(categoriaModel.getId());

        TextView tvEmpty = (TextView) findViewById(android.R.id.empty);
        lista = (ListView) findViewById(R.id.listView2);

        tvEmpty.setVisibility(TextView.GONE);

        //Collections.sort(listItemCategoria, new ItemCategoriaComparator()); //ordena os dados

        adapterItemCategoria = new adapterItemCategoria(this, R.layout.listitems_item_categoria, listItemCategoria);

        lista.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lista.setAdapter(adapterItemCategoria);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ItemCategoriaModel itemCategoriaModel = (ItemCategoriaModel) (lista.getItemAtPosition(position));

                Intent myIntent = new Intent(getApplication(), viewItemCategoriaDetail.class);
                myIntent.putExtra("itemCategoriaId", String.valueOf(itemCategoriaModel.getId()) );
                startActivity(myIntent);
            }
        });

        if(listItemCategoria.size() == 0){
            tvEmpty.setVisibility(TextView.VISIBLE);
        }

        lista.setEmptyView(tvEmpty);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item_categoria_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.deletar_selecionados) {

            int count = lista.getChildCount();

            for (int i = 0; i < count; i++) {
                View child = lista.getChildAt(i);
                CheckBox cb = (CheckBox) child.findViewById(R.id.checkBoxDeletar);

                if(cb.getVisibility() == CheckBox.VISIBLE){
                    db = new BDHelper(getApplicationContext());
                    if(cb.isChecked()){
                        listItemCategoriaChecked.add(adapterItemCategoria.getItem(i).getId());
                    }
                    cb.setVisibility(CheckBox.GONE);
                }else{
                    listItemCategoriaChecked = new ArrayList<Integer>();
                    if(listItemCategoriaChecked.size() > 0){
                        listItemCategoriaChecked.clear();
                    }
                    cb.setChecked(false);
                    cb.setVisibility(CheckBox.VISIBLE);
                }
            }

            if(listItemCategoriaChecked.size() > 0){
                db.deleteAllItemCategoriaChecked(listItemCategoriaChecked);
                adapterItemCategoria.clear();
                adapterItemCategoria.updateDataList(db.getAllItemCategoriasList(categoriaModel.getId()));
            }
        }

        return true;
    }
}
